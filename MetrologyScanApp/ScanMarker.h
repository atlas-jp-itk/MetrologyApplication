#pragma once

#include "CoordinateSystem.h"
#include <iostream>
#include <string>
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Drawing;


namespace MoveScanMarker {
public ref class ScanMarker
	{	
	private: CoordinateSystem ^cs0;
	private: std::string *sensortype;
	public: float pitchX;
	public: float pitchY;
	private: PictureBox^ pictureBox;
	private: Bitmap^ canvas;
	private: Brush^ brushColor;
	private: float canvasSizeX;
	private: float canvasSizeY;
	private: float canvasSize;
	private: float positionX;
	private: float positionY;
	private: float previousX;
	private: float previousY;
	private: float directionX;
	private: float directionY;
	private: float radius;
	private: Graphics^ g;
//	private: float* getRelativePos(float xx,float yy, float *pos);
//	private: float* getPicturePos(float xx, float yy, float *pos);
	private: Pen ^ reddotPen;
	private: Pen ^ blackPen;

	public: ScanMarker::ScanMarker();
	public: ScanMarker::ScanMarker(PictureBox^ pb, Bitmap^ cv, Brush^ cl, CoordinateSystem^ cs);
	public: void PutCircle(float x, float y);
	public: void ChagePreviousCircle();
	public: void DrawBase();
	public: void DrawSingleLine(Pen^ pen, float x1, float y1, float x2, float y2);
	public:	void DrawRectangleLines(Pen^ pen, float x1, float y1, float x2, float y2);
	public: void SetSensorType(std::string type) {
		*sensortype = type;
	}

	};

}		