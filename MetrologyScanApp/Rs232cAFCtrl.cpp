#include "stdafx.h"
#include "Rs232cAFCtrl.h"
#include <sstream>


Rs232cAFCtrl::Rs232cAFCtrl()
{
	m_prsMain = NULL;
	m_AFStatus[0] = '\0';
	m_AFZoom[0] = '\0';
	m_AFZpos = -1;
	m_AFZposum = -1;
}
Rs232cAFCtrl::~Rs232cAFCtrl()
{
	m_prsMain->CloseCommPort();
}
// -----------------------------------------------------------------
// 		RS�232C Initialize
// -----------------------------------------------------------------
int Rs232cAFCtrl::RS_Setup(int m_nPort)
{
	if (m_prsMain)return 0;
	m_prsMain = new CRs232c;


	std::stringstream ss; ss.str("");
//	ss << "COM" << m_nPort << "";
	ss << "\\\\.\\COM" << m_nPort << "";

	if (!m_prsMain->OpenCommPort((char*)ss.str().c_str(), 2049, 2049)) return 1;


	if (!m_prsMain->InitCommPort("baud=19200 parity=n data=8 stop=2 xon=on"))return 3;		// Parameter failed
/*
	DCB CommDcb;
	COMMTIMEOUTS CommTimeOuts;

	CommTimeOuts.ReadIntervalTimeout = 5;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 0;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 5;
	CommTimeOuts.WriteTotalTimeoutConstant = 50;
	*/
//	CommDcb.DCBlength = sizeof(DCB);		/* DCB structure size */
//	CommDcb.BaudRate = 192000;	/* Speed */
//	CommDcb.fBinary = TRUE;				/* Binary mode */
//	CommDcb.fParity = TRUE;				/* Parity check */
//	CommDcb.ByteSize = 8;	/* Bit length */
//	CommDcb.Parity = 0;		/* Parity */
//	CommDcb.StopBits = 2;	/* Stop bit */

//	if (!m_prsMain->InitCommPort(CommTimeOuts,CommDcb))return 3;		// Parameter failed

	return 0;
}
void Rs232cAFCtrl::RS_Reset(void)
{
	if (m_prsMain) {
		m_prsMain->CloseCommPort();
		delete m_prsMain;
	}
	m_prsMain = NULL;
}
// -----------------------------------------------------------------
// 		Send AF ( String )
// -----------------------------------------------------------------
void Rs232cAFCtrl::WriteStringMsc(char *cCmd)
{

	int n = strlen(cCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(cCmd, n);
	}
}

// -----------------------------------------------------------------
// 		Recieve AF
// -----------------------------------------------------------------
int Rs232cAFCtrl::ReadMsc(char *m_strResp, unsigned int pt)
{
	int		rc;
	char	sDummy[2];

	if (m_prsMain) {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = 0;
		m_prsMain->ReadCommBlock(m_strResp, pt);	// Read
		while (!m_prsMain->ReadComm(1, sDummy));
	}
	else {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = '0';
	}
	return 0;
}
// -----------------------------------------------------------------
// 		Send AF
// -----------------------------------------------------------------
void Rs232cAFCtrl::WriteMsc(void)
{

	int n = strlen(m_strCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(m_strCmd, n);
	}
}

char*  Rs232cAFCtrl::CheckStatus() {
	m_AFStatus[0] = '\0';
	/* Receive Data */
	char pszData[256];		/* Buffer for Data */
	char pszDataTmp[256];	/* Buffer for Data */
	BOOL bFlag;			/* Flag */
	DWORD dwSize;		/* Size of Read/Write Data */
	DWORD i;			/* Loop Counter */
	bFlag = FALSE;
	pszData[0] = '\0';

	i = 0;
	int j = 0;
	dwSize = 0;
	do {
//		std::cout << "reading data... " << i << std::endl;
		/* Receive Data */
		j = 0;
		while ((dwSize = m_prsMain->ReadCommBlock(pszDataTmp, 1)) == 0 && j<256) {
			j++;
			Sleep(100);
		}
//		std::cout << j  << " " << dwSize << " " << pszData << std::endl;
		if (j == 256) {
			strncpy_s(m_AFStatus,"Timeout",255);
			return m_AFStatus;
		}
		if (dwSize == 1) {
			pszData[i] = pszDataTmp[0];
			pszData[i + 1] = '\0';
			i++;
			/* Compare Data */
			if (0 == strncmp(pszData, pszCE, 255)) {
				strncpy_s(pszDataTmp, pszCEMessage, 255);
				bFlag = TRUE;
			}
			else if (0 == strncmp(pszData, pszFE, 255)) {
				strncpy_s(pszDataTmp, pszFEMessage, 255);
				bFlag = TRUE;
			}
			else if (0 == strncmp(pszData, pszPE, 255)) {
				strncpy_s(pszDataTmp, pszPEMessage, 255);
				bFlag = TRUE;
			}
			else if (0 == strncmp(pszData, pszJ, 255)) {
				strncpy_s(pszDataTmp, pszJMessage, 255);
				bFlag = TRUE;
			}
			else if (0 == strncmp(pszData, pszJF, 255)) {
				strncpy_s(pszDataTmp, pszJFMessage, 255);
				bFlag = TRUE;
			}
			else if (0 == strncmp(pszData, pszJN, 255)) {
				strncpy_s(pszDataTmp, pszJNMessage, 255);
				bFlag = TRUE;
			}
			else {
				if (i > 2) {
					if ((pszData[i - 2] == CR)
						& (pszData[i - 1] == LF)) {
						pszData[0] = '\0';
						i = 0;
					}
				}
			}
		}
	} while ((FALSE == bFlag) && (i < 255));
//	std::cout << pszDataTmp << std::endl;
//	std::cout << j << std::endl;
	if (j == 255) {
		strncpy_s(m_AFStatus, "Error", 255);
		return m_AFStatus;
	}
//	std::cout << pszDataTmp << std::endl;
	strncpy_s(m_AFStatus, pszDataTmp, 255);
//	std::cout << m_AFStatus << std::endl;
	return m_AFStatus;
}
int Rs232cAFCtrl::ClearAF() {
	strncpy_s(m_strCmd, sizeof(m_strCmd), "Q", 1);
	sprintf_s(m_strCmd + 1, sizeof(m_strCmd), "%c", CR);
	WriteMsc();
	Sleep(100);

	return 0;
}
int Rs232cAFCtrl::ResumeAF() {
	Sleep(100);
	strncpy_s(m_strCmd, sizeof(m_strCmd), "AF0", 3);
	sprintf_s(m_strCmd + 3, sizeof(m_strCmd), "%c", CR);
	WriteMsc();
	Sleep(100);

	return 0;
}
int Rs232cAFCtrl::SendSC0(int n) {

	for (int ii = 0; ii < n; ii++) {
		ClearAF();
		strncpy_s(m_strCmd, sizeof(m_strCmd), "SC0", 3);
		sprintf_s(m_strCmd + 3, sizeof(m_strCmd), "%c", CR);
		int n = strlen(m_strCmd);
		std::cout << "sending command " << (char*)m_strCmd << std::endl;
		WriteMsc();
		Sleep(300);
    
		int dwSize;
		int i=0;
		char r[1];
		do {
			if (i > 256)return -2;
			//		std::cout << "reading data... " << i << std::endl;
			//////  Receive Data 
			if ((dwSize = m_prsMain->ReadCommBlock(r, 1)) == 0) return -1;
			if (dwSize == 1) {
//				std::cout << r[0] << std::endl;
			}
			i++;
//			Sleep(10);
		} while (r[0] != 'K');
		if (r[0] == 'K') std::cout << " OK " << std::endl;
		else std::cout << " NG " << std::endl;
	}

//	std::cout << "sc0 successfully done " << std::endl;
	Sleep(100);
	
	return 0;
}
int Rs232cAFCtrl::MoveZpos(char dir, int pulse) {
	ClearAF();
	strncpy_s(m_strCmd, sizeof(m_strCmd), "", 0);
	sprintf_s(m_strCmd, sizeof(m_strCmd), "%c:%d%c", dir, pulse, CR);
//	strncpy_s(m_strCmd, sizeof(m_strCmd), "F:50", 4);
//	sprintf_s(m_strCmd+4, sizeof(m_strCmd), "%c", CR);
	std::cout << "sending command " << (char*)m_strCmd << std::endl;
	WriteMsc();
	Sleep(300);

	int dwSize;
	int i = 0;
	char r[1];
	do {
		if (i > 256)return -2;
		//		std::cout << "reading data... " << i << std::endl;
		//////  Receive Data 
		if ((dwSize = m_prsMain->ReadCommBlock(r, 1)) == 0) return -1;
//		if (dwSize == 1) {
//			std::cout << r[0] << std::endl;
//		}
		i++;
//		Sleep(10);
	} while (r[0] != 'K');
	if (r[0] == 'K') std::cout << " OK " << std::endl;
	else std::cout << " NG " << std::endl;

	Sleep(100);

	return 0;
}
int Rs232cAFCtrl::GetZPos() {

	ClearAF();
	strncpy_s(m_strCmd, sizeof(m_strCmd), "DP", 2);
	sprintf_s(m_strCmd + 2, sizeof(m_strCmd), "%c", CR);
	WriteMsc();
	Sleep(100);	// Wait	

	/* Receive Data */
	char pszData[256];		/* Buffer for Data */
	char pszDataTmp[256];	/* Buffer for Data */
	BOOL bFlag;			/* Flag */
	DWORD dwSize;		/* Size of Read/Write Data */
	DWORD i;			/* Loop Counter */
	bFlag = FALSE;
	pszData[0] = '\0';

	i = 0;
	dwSize = 0;
	do {
		//		std::cout << "reading data... " << i << std::endl;
				/* Receive Data */
		if ((dwSize = m_prsMain->ReadCommBlock(pszDataTmp, 1)) == 0) {
			m_AFZpos = -1;
			return -1;
		}
		if (dwSize == 1) {
			pszData[i] = pszDataTmp[0];
			pszData[i + 1] = '\0';
			i++;
			/* Compare Data */
			if (strlen(pszData)==9) {
				bFlag = true;
			}
			else {
				if (i > 2) {
					if ((pszData[i - 2] == CR)
						& (pszData[i - 1] == LF)) {
						pszData[0] = '\0';
						i = 0;
					}
				}
			}
		}
	} while ((FALSE == bFlag) & (i < 255));
	m_AFZpos = atoi(pszData);
	m_AFZposum = (float)m_AFZpos * (float)UMPARPULSEAF;
//	ResumeAF();
	return m_AFZpos;
}
int Rs232cAFCtrl::SetZoom(int x) {
	ClearAF();
	if (x == 2) {
		strncpy_s(m_strCmd, sizeof(m_strCmd), "XA", 2);
		sprintf_s(m_strCmd + 2, sizeof(m_strCmd), "%c", CR);
	}
	else if (x == 5) {
		strncpy_s(m_strCmd, sizeof(m_strCmd), "XB", 2);
		sprintf_s(m_strCmd + 2, sizeof(m_strCmd), "%c", CR);
	}
	else if (x == 10) {
		strncpy_s(m_strCmd, sizeof(m_strCmd), "XC", 2);
		sprintf_s(m_strCmd + 2, sizeof(m_strCmd), "%c", CR);
	}
//	std::cout << m_strCmd << std::endl;
	WriteMsc();
	Sleep(100);

	int dwSize;
	int i = 0;
	char r[1];
	do {
		if (i > 256)return -2;
		//		std::cout << "reading data... " << i << std::endl;
		//////  Receive Data 
		if ((dwSize = m_prsMain->ReadCommBlock(r, 1)) == 0) return -1;
		if (dwSize == 1) {
			//				std::cout << r[0] << std::endl;
		}
		i++;
		//			Sleep(100);
	} while (r[0] != 'K');
	if (r[0] == 'K') std::cout << " OK " << std::endl;
	else std::cout << " NG " << std::endl;

	//	std::cout << "sc0 successfully done " << std::endl;
	Sleep(200);
//	ResumeAF();
	return 0;

}
std::string  Rs232cAFCtrl::CheckZoom() {
	ClearAF();
	strncpy_s(m_strCmd, sizeof(m_strCmd), "DX", 2);
	sprintf_s(m_strCmd + 2, sizeof(m_strCmd), "%c", CR);
	std::cout << "sending command " << (char*)m_strCmd << std::endl;
	WriteMsc();
	Sleep(100);

	/* Receive Data */
	bool iszoomdata = false;
	int dwSize;
	int i = 0;
	char r[1];
	char zoom[5];
	do {
		if (i > 256) {
			strncpy_s(m_AFZoom, "Err", 255);
			return m_AFZoom;
		}
		//		std::cout << "reading data... " << i << std::endl;
		//////  Receive Data 
		if ((dwSize = m_prsMain->ReadCommBlock(r, 1)) == 0) {
			strncpy_s(m_AFZoom, "Err", 255);
			return m_AFZoom;
		}
		if (dwSize == 1) {
			if (!iszoomdata && r[0] != 'X')continue;
			else iszoomdata = true;
			zoom[i] = r[0];
			zoom[i + 1] = '\0';
		}
		i++;
		Sleep(100);
	} while (!(iszoomdata == true && r[0] == LF));
	zoom[3] = '\0';	zoom[4] = '\0';
	strncpy_s(m_AFZoom,zoom, 255);
//	ResumeAF();
	return zoom;
}

int Rs232cAFCtrl::AFTest() {
	if (int iret=SendSC0(1) != 0) {
		std::cout << "returned " << iret << std::endl;
	}
 
	Sleep(100);
	CheckStatus();
	std::cout << "Status    : " << m_AFStatus << std::endl;
	GetZPos();
	std::cout << "Zpos      : " << m_AFZpos << std::endl;

	CheckZoom();
	std::cout << "Zoom Lens :" << m_AFZoom << std::endl;
	SetZoom(10);
	CheckZoom();
	std::cout << "Zoom Lens :" << m_AFZoom << std::endl;


	if (int iret = SendSC0(1) != 0) {
		std::cout << "returned " << iret << std::endl;
	}
	std::cout << "AFTest() finished." << std::endl;

	return 0;
}
