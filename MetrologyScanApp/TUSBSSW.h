
#ifdef TUSBSSW_EXPORTS
#define TUSBSSW_API __declspec(dllexport) __cdecl
#else
#define TUSBSSW_API __declspec(dllimport) __cdecl
#endif

#ifdef __cplusplus
extern "C"{
#endif

	short TUSBSSW_API Tusbs01sw_Device_Open( short id );
	void TUSBSSW_API Tusbs01sw_Device_Close( short id );
	short TUSBSSW_API Tusbs01sw_Out( short id ,char OnOff);
	short TUSBSSW_API Tusbs01sw_In( short id ,char *OnOff);

#ifdef __cplusplus
}
#endif

