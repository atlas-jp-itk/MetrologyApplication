#pragma once
#include <windows.h>
#include <iostream>
#include "tusbssw.h"
#include <string.h>
#include "stdafx.h"
#include "CoordinateSystem.h"
#include "TurtleSwitch.h"
#include "Rs232cXYCtrl.h"
#include "Rs232cAFCtrl.h"
#include "Rs232cRVCtrl.h"
#include "Rs232cWLCtrl.h"
#include "sstream"
#include "DataHandler.h"
#include "OnlineMonitor.h"
#include "ImageAnalysis.h"


#define COMXY 6
#define COMAF 3
#define COMRV 7
#define COMWL 4

#define COM_MAX 256

ref class MetrologyScan
{
private:
	TurtleSwitch *sw0;
	Rs232cXYCtrl *rsc0; // Rs232c Controler for XY stage
	Rs232cAFCtrl *rsc1; // Rs232c Controler for AF
	Rs232cRVCtrl *rsc2; // Rs232c Controler for Revolver
	Rs232cWLCtrl *rsc3; // Rs232c Controler for Wafer Loader

	int StagePosition; // 0 : unknown  1: scanning area  2: wafer load position
	double currentposx;
	double currentposy;
	int currentzoom;
	int m_COMPortXY;
	int m_COMPortAF;
	int m_COMPortRV;
	int m_COMPortWL;
	DataHandler *dh;
	OnlineMonitor *om;
	int scanmode;
	bool docloseplot;
//	ImageAnalysis *imatool;

public:
	CoordinateSystem ^cs0;
	MetrologyScan(); 
	DataHandler* getDataHandler() { return dh; }
	Rs232cXYCtrl * getRs232cXYCtrl() { return rsc0; }
	Rs232cAFCtrl * getRs232cAFCtrl() { return rsc1; }
	Rs232cRVCtrl * getRs232cRVCtrl() { return rsc2; }
	Rs232cWLCtrl * getRs232cWLCtrl() { return rsc3; }
	TurtleSwitch * getTurtleSwitch() { return sw0; }
	void SetScanningPosition() { StagePosition = 1; }
	void SetWaferLoadPosition() { StagePosition = 2; }
	int GetStagePosition() { return StagePosition; }
	void SetCOMPorts(int comxy, int comaf, int comrv, int comwl) {
		m_COMPortXY = comxy;
		m_COMPortAF = comaf;
		m_COMPortRV = comrv;
		m_COMPortWL = comwl;
	}
	void SetScanMode(int type) { scanmode = type; }
	void SetZoom(int zoom);
	int GetZoom() { return currentzoom; }
	int GetScanmode() { return scanmode; }
	void SetClosePlot(bool ko) { docloseplot = ko; }
	bool GetClosePlot() { return docloseplot; }
	int OpenApplication(char * cmd);
	int InitializeScan(float _picH, float _picW, float scanorgx, float scanorgy);
	int PatternMatchingScan();
	int RunScan(float startx, float starty, float stepx, float stepy, float endx, float endy);
	void CheckScanMatrix(float &_startx, float &_starty, float &_stepx, float &_stepy, float &_stopx, float &_stopy);
	int LoadOneTray(int itrey);
	int UnLoadOneTray(int itrey);
	int VacuumControl(int state);
	

//	void FileHandling();
//	void ROOTtest();
};

