// ******************************************************************************
//
// ******************************************************************************
#include	"stdafx.h"
#include	<conio.h>
#include	<string.h>
#include	<math.h>
#include    "Windows.h"
#include    <iostream>
#include    "Rs232cXYCtrl.h"
#include    "sstream"
#include    <iostream>


Rs232cXYCtrl::Rs232cXYCtrl()
{
	m_prsMain = NULL;
}


Rs232cXYCtrl::~Rs232cXYCtrl()
{
}

// -----------------------------------------------------------------
// 		RSｰ232C Initialize
// -----------------------------------------------------------------
int Rs232cXYCtrl::RS_Setup(int m_nPort)
{
	if (m_prsMain)return 0;
	m_prsMain = new CRs232c;


	std::stringstream ss; ss.str("");
//	ss << "COM" << m_nPort << "";
	ss << "\\\\.\\COM" << m_nPort << "";

	if (!m_prsMain->OpenCommPort((char*)ss.str().c_str(), 2049, 2049)) return 1;

	INIT_COMM_PAC pac;
	pac.BaudRate = 9600;	// 9600bout
	pac.ByteSize = 8;		// 8bits
	pac.Parity = 0;		// non parity
	pac.StopBits = 0;		// 1 stop bit
	if (!m_prsMain->InitCommPort(&pac))return 3;		// Parameter failed

	//	sprintf_s(m_strCmd, sizeof(m_strCmd), "P:19P4%c%c", CR, LF);
	//  WriteMsc();

	return 0;
}

// -----------------------------------------------------------------
// 		RSｰ232C Close
// -----------------------------------------------------------------
void Rs232cXYCtrl::RS_Reset(void)
{
	if (m_prsMain) {
		m_prsMain->CloseCommPort();
		delete m_prsMain;
	}
	m_prsMain = NULL;
}

// -----------------------------------------------------------------
// 		Send QT ( String )
// -----------------------------------------------------------------
void Rs232cXYCtrl::WriteStringMsc(char *cCmd)
{

	int n = strlen(cCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(cCmd, n);
	}
}

// -----------------------------------------------------------------
// 		Recieve QT
// -----------------------------------------------------------------
int Rs232cXYCtrl::ReadMsc(char *m_strResp, unsigned int pt)
{
	int		rc;
	char	sDummy[2];

	if (m_prsMain) {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = 0;
		m_prsMain->ReadCommBlock(m_strResp, pt);	// Read
		while (!m_prsMain->ReadComm(1, sDummy));
	}
	else {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = '0';
	}
	return 0;
}


// -----------------------------------------------------------------
// 		ここから先はSimple版では使用しない。
// -----------------------------------------------------------------

// -----------------------------------------------------------------
// 		Send QT
// -----------------------------------------------------------------
void Rs232cXYCtrl::WriteMsc(void)
{

	int n = strlen(m_strCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(m_strCmd, n);
	}
}

// -----------------------------------------------------------------
// 		Origin
//　	Axis: Move axis 1:A-axis 2:B-axis
// -----------------------------------------------------------------
int Rs232cXYCtrl::Origin(int nAxis)
{
	int			ecnt = 0;

	switch (nAxis) {
//	case 1: sprintf_s(m_strCmd, sizeof(m_strCmd), "H:A%c%c", CR, LF); break;
//	case 2: sprintf_s(m_strCmd, sizeof(m_strCmd), "H:B%c%c", CR, LF); break;
	case 1: sprintf_s(m_strCmd, sizeof(m_strCmd), "AGO:A0%c%c", CR, LF); break;
	case 2: sprintf_s(m_strCmd, sizeof(m_strCmd), "AGO:B0%c%c", CR, LF); break;
	}
	WriteMsc();
	do {
		MscSts(nAxis);
		if (ecnt++ > ErrRetry) return 1;
	} while (m_bBusy);
	return 0;
}

int Rs232cXYCtrl::GoHome() {
	int			ecnt = 0;

	sprintf_s(m_strCmd, sizeof(m_strCmd), "H:AB%c%c", CR, LF);
	WriteMsc();
	do {
		if (ecnt++ > ErrRetry) return 1;
	} while (m_bBusy);
	return 0;
}
// -----------------------------------------------------------------
// 		ABS
//　	Axis: Move axis 1:A-axis 2:B-axis
//　	fval: Move pulse
// -----------------------------------------------------------------
int Rs232cXYCtrl::Absolute(int nAxis, float fval)
{
	int	ecnt = 0;

	switch (nAxis) {
	case 1: strncpy_s(m_strCmd, sizeof(m_strCmd), "AGO:A", 5); break;
	case 2: strncpy_s(m_strCmd, sizeof(m_strCmd), "AGO:B", 5); break;
	}
	sprintf_s(m_strCmd + 5, sizeof(m_strCmd), "%.0f%c%c", fval, CR, LF);
	WriteMsc();
	do {
		MscSts(nAxis);
		if (ecnt++ > ErrRetry) return 1;
	} while (m_bBusy);
	return 0;
}
int Rs232cXYCtrl::Absolute2D(float fvalx, float fvaly)
{
	int	ecnt = 0;
	strncpy_s(m_strCmd, sizeof(m_strCmd), "AGO:", 4);
	sprintf_s(m_strCmd + 4, sizeof(m_strCmd), "A%.0fB%.0f%c%c", fvalx, fvaly, CR, LF);
	WriteMsc();
	for (int nAxis = 1; nAxis <= 2; nAxis++) {
		do {
			MscSts(nAxis);

			if (ecnt++ > ErrRetry) return 1;
		} while (m_bBusy);
	}		

	if (fvalx != m_fAValue || fvaly != m_fBValue) {
		std::cerr << "actual position is not the same as target position" << std::endl;
		std::cerr << "target(x,y)=(" << fvalx << "," << fvaly << ")   Actual(x,y)=(" << m_fAValue  << "," << m_fBValue << ")" << std::endl;

		return -1;
	}

	return 0;
}

// -----------------------------------------------------------------
// 		REL Plus
//　	Axis: Move axis 1:A-axis 2:B-axis
//　	fval: Move pulse
// -----------------------------------------------------------------
int Rs232cXYCtrl::RelativeP(int nAxis, float fval)
{
	int	ecnt = 0;

	switch (nAxis) {
	case 1: strncpy_s(m_strCmd, sizeof(m_strCmd), "MGO:A", 5); break;
	case 2: strncpy_s(m_strCmd, sizeof(m_strCmd), "MGO:B", 5); break;
	}
	sprintf_s(m_strCmd + 5, sizeof(m_strCmd), "%.0f%c%c", fval, CR, LF);
	WriteMsc();
	do {
		MscSts(nAxis);
		if (ecnt++ > ErrRetry) return 1;
	} while (m_bBusy);
	return 0;
}

// -----------------------------------------------------------------
// 		REL Minus
//　	Axis: Move axis 1:A-axis 2:B-axis
//　	fval: Move pulse
// -----------------------------------------------------------------
int Rs232cXYCtrl::RelativeM(int nAxis, float fval)
{
	int	ecnt = 0;

	switch (nAxis) {
	case 1: strncpy_s(m_strCmd, sizeof(m_strCmd), "MGO:A", 5); break;
	case 2: strncpy_s(m_strCmd, sizeof(m_strCmd), "MGO:B", 5); break;
	}
	float fv = fval * -1.0f;
	sprintf_s(m_strCmd + 5, sizeof(m_strCmd), "%.0f%c%c", fv, CR, LF);
	WriteMsc();
	do {
		MscSts(nAxis);
		if (ecnt++ > ErrRetry) return 1;
	} while (m_bBusy);
	return 0;
}

// -----------------------------------------------------------------
// 		Read current position and status
//　	Axis: Move axis 1:A-axis 2:B-axis
// -----------------------------------------------------------------
int Rs232cXYCtrl::MscSts(int nAxis)
{
	int	ecnt = 0;
	int	nRcnt, nlen;
	switch (nAxis) {
	case 1: sprintf_s(m_strCmd, sizeof(m_strCmd), "Q:A0%c%c", CR, LF); break;
	case 2: sprintf_s(m_strCmd, sizeof(m_strCmd), "Q:B0%c%c", CR, LF); break;
	}
	do {
		WriteMsc();
		if (ecnt++ > ErrRetry) return -1;
		Sleep(100);	// Wait
		if ((nRcnt = ReadMsc(m_strResp, 12)) != 0) return -2;
		nlen = strlen(m_strResp);
	} while (nRcnt != 0 || nlen != 12);
	switch (nAxis) {
	case 1: m_fAValue = float(atof(m_strResp)); break;
	case 2: m_fBValue = float(atof(m_strResp)); break;
	}
	nlen = strlen(m_strResp);
	for (int i = 9; i < nlen; i++) {
		if (m_strResp[i] == 'D') { m_bBusy = TRUE; return 1; }
		if (m_strResp[i] == 'K') { m_bBusy = FALSE; return 0; }
		if (m_strResp[i] == 'L') { m_bBusy = FALSE; return 2; }
	}
	return (int)m_bBusy;
}

// -----------------------------------------------------------------
// 		Read current position only
// -----------------------------------------------------------------
int Rs232cXYCtrl::MscPos(void)
{
	int	ecnt = 0;
	int	nRcnt, nlen;
	sprintf_s(m_strCmd, sizeof(m_strCmd), "Q:1%c%c", CR, LF);
	do {
		WriteMsc();
		if (ecnt++ > ErrRetry) return 1;
		Sleep(100);	// Wait
		if ((nRcnt = ReadMsc(m_strResp, 21)) != 0) return 1;
		nlen = strlen(m_strResp);
	} while (nRcnt != 0 || nlen != 21);
	m_fAValue = float(atof(m_strResp));
	m_fBValue = float(atof(m_strResp + 10));
	return 0;
}

// -----------------------------------------------------------------
// 		Sensor status read.
// -----------------------------------------------------------------
int Rs232cXYCtrl::MscSensor(void)
{
	int	ecnt = 0;
	int	nRcnt, nlen;

	sprintf_s(m_strCmd, sizeof(m_strCmd), "I:%c%c", CR, LF);
	do {
		WriteMsc();
		if (ecnt++ > ErrRetry) return 1;
		Sleep(100);
		if ((nRcnt = ReadMsc(m_strResp, 11)) != 0) return 1;
		nlen = strlen(m_strResp);
	} while (nRcnt != 0 || nlen != 11);

	if (m_strResp[0] == '1') m_bALimitPlus = TRUE;
	else m_bALimitPlus = FALSE;
	if (m_strResp[1] == '1') m_bALimitMinus = TRUE;
	else m_bALimitMinus = FALSE;
	if (m_strResp[2] == '1') m_bABeforeOrigin = TRUE;
	else m_bABeforeOrigin = FALSE;
	if (m_strResp[3] == '1') m_bAOrigin = TRUE;
	else m_bAOrigin = FALSE;
	if (m_strResp[5] == '1') m_bBLimitPlus = TRUE;
	else m_bBLimitPlus = FALSE;
	if (m_strResp[6] == '1') m_bBLimitMinus = TRUE;
	else m_bBLimitMinus = FALSE;
	if (m_strResp[7] == '1') m_bBBeforeOrigin = TRUE;
	else m_bBBeforeOrigin = FALSE;
	if (m_strResp[8] == '1') m_bBOrigin = TRUE;
	else m_bBOrigin = FALSE;

	return 0;
}

void Rs232cXYCtrl::PrintPosition() {
	std::cout << "pos (x, y)=(" << m_fAValue << ", " << m_fBValue << ")" << std::endl;
}