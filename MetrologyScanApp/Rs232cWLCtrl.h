#pragma once


#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include "Rs232c.h"
#include "WaferLoaderErrors.h"

#define		LF			0x0a
#define		CR			0x0d
#define		NUL			0x00
#define		ErrRetry	3000
#define     QU          0x3F

class Rs232cWLCtrl
{
	DCB		dcb;								// RS-232C Port
	COMSTAT	ComStat;
	int		IdCom, comerr;
	char	m_strCmd[2000];                       // Send Character buffer

	void 	WriteMsc(void);					// Send MSC
	int m_VACS; // stage vacumme status
	int m_ORGS; // Orig status [0]... All unit orig status [1]...Robot Orig status [2]... Aligner Orig status
	int m_TS[25];  // Tray Status 0: tray empty  1: tray exist 2:unknown   3: measured
	int m_CS;      // cassete status 0: tray empty  1: tray exist  2: unknown
	int m_arm;     // 0: empty 1: wafer exist  2: unknown
	int m_aligner; // 0: empty 1: wafer exist  2: unknown
	int m_XYstage; // 0: empty 1: wafer exist  2: unknown
	WLErrorHandler *eh;

public:
	Rs232cWLCtrl();
	~Rs232cWLCtrl();
	int     Initialization();
	int GetVACS() { return m_VACS; }
	int GetORGS() { return m_ORGS; }
	int GetTS(int num) { return m_TS[num]; }  // Tray Status 0: tray empty  1: tray exist 2:unknown   3: measuring   4:  measured
	int GetCS() { return m_CS; }      // cassete status 0: tray empty  1: tray exist  2: unknown
	int Getarm() { return m_arm; }     // 0: empty 1: wafer exist  2: unknown
	int Getaligner() { return m_aligner; } // 0: empty 1: wafer exist  2: unknown
	int GetXYstage() { return m_XYstage; } // 0: empty 1: wafer exist  2: unknown
	void SetTS(int num, int stat) { m_TS[num] = stat; }
	char	m_strResp[1000];                      // Recive character buffer
	char	m_strDummy[5];                      // Recive character buffer
	CRs232c *m_prsMain;							// RS-232C Class
	int		RS_Setup(int m_nPort);			// RS Initialize
	void	RS_Reset(void);					// RS Close
	int		ReadMsc(char *m_strResp, unsigned int pt);
	int		ReadMsc(char *m_strResp);
	void	WriteStringMsc(char *cCmd);		// Send string MSC
	int     SendReceiveCommand(std::string cmd, int iwait, bool print);
	int     WaferLoaderTest();
	int     CheckStatus(bool domapping, bool print);
	int     doMapping();
	std::string GetError();

};
