#include "stdafx.h"
#include "CoordinateSystem.h"


CoordinateSystem::CoordinateSystem(float _picH, float _picW, float scanorgx, float scanorgy)
{
	setCoordination(_picH, _picW, scanorgx, scanorgy);
}
void CoordinateSystem::setCoordination(float _picH, float _picW, float scanorgx /*[pls]*/, float scanorgy/*[pls]*/) {
	pictureH = _picH;
	pictureW = _picW;
	pictureSize = pictureH > pictureW ? pictureW : pictureH;
	scanOrigX = scanorgx * abs(UMPARPULSEX);
	scanOrigY = scanorgy * abs(UMPARPULSEY);
	float posmin[2] = { 0.,0. };
	float posmax[2] = { 0.,0. };
	getAxisFromScanPos(XAXISMIN, YAXISMIN, posmin);
	getAxisFromScanPos(XAXISMAX, YAXISMAX, posmax);
	if (UMPARPULSEX > 0) {
		xposmin = posmin[0];
		xposmax = posmax[0];
	}
	else {
		xposmin = posmax[0];
		xposmax = posmin[0];
	}
	if (UMPARPULSEY > 0) {
		yposmin = posmin[1];
		yposmax = posmax[1];
	}
	else {
		yposmin = posmax[1];
		yposmax = posmin[1];
	}
	scale = pictureSize / WAFERSIZE;
}

void CoordinateSystem::getScanPosFromAxis(float xx, float yy, float * pos) {
	pos[0] = (xx - scanOrigX - scanOrigdX) / UMPARPULSEX;
	pos[1] = (yy - scanOrigY - scanOrigdY) / UMPARPULSEY;
}
void CoordinateSystem::getAxisFromScanPos(float aa, float bb, float * pos) {
	pos[0] = aa * UMPARPULSEX + scanOrigX + scanOrigdX;
	pos[1] = bb * UMPARPULSEY + scanOrigY + scanOrigdY;
}
void CoordinateSystem::getPicturePosFromScanPos(float aa, float bb, float * pos) {

	pos[0] = -1 * (aa * UMPARPULSEX + scanOrigX) * scale + pictureW / 2;
	pos[1] = (bb * UMPARPULSEY + scanOrigY) * scale + pictureH / 2 + scale * ORIFLA / 2;
}
void CoordinateSystem::getPicturePosFromAxis(float xx, float yy, float * pos) {
	pos[0] = xx * scale + pictureW / 2;
	pos[1] = yy * scale + pictureH / 2 + scale * ORIFLA / 2;
}
