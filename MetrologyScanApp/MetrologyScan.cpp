#include "stdafx.h"
#include "MetrologyScan.h"
#include <string>
#include <fstream>


MetrologyScan::MetrologyScan()
{
	sw0 = new TurtleSwitch(0);
	rsc0 = new Rs232cXYCtrl();	
	rsc1 = new Rs232cAFCtrl();
	rsc2 = new Rs232cRVCtrl();
	rsc3 = new Rs232cWLCtrl();
	cs0 = gcnew CoordinateSystem();
	dh = new DataHandler();
	om = new OnlineMonitor();
//	imatool = new ImageAnalysis();
	scanmode = 0;
	docloseplot = true;
	dh->FileHandling();
	StagePosition = 0;
}
int MetrologyScan::InitializeScan(float _picH, float _picW, float scanorgx, float scanorgy) {

	bool isSimpleBoot = true;
	bool WLonlytest = false;
	cs0->setCoordination(_picH, _picW, scanorgx, scanorgy);
	if (!WLonlytest) {
		std::cout << "Opening Rs232c Serial port for XY stage" << std::endl;
		if ((rsc0->RS_Setup(m_COMPortXY)) != 0)
		{
			std::cout << "RS-232C cannot open port (COM" << m_COMPortXY << ")" << std::endl;
			return FALSE;
		}
		std::cout << "Opening Rs232c Serial port for Auto Focus" << std::endl;
		if ((rsc1->RS_Setup(m_COMPortAF)) != 0)
		{
			std::cout << "RS-232C cannot open port (COM" << m_COMPortAF << ")" << std::endl;
			return FALSE;
		}
		std::cout << "Opening Rs232c Serial port for Revolver" << std::endl;

		if ((rsc2->RS_Setup(m_COMPortRV)) != 0)
		{
			std::cout << "RS-232C cannot open port (COM" << m_COMPortRV << ")" << std::endl;
			return FALSE;
		}
	}
	std::cout << "Opening Rs232c Serial port for Wafer Loader" << std::endl;

	if ((rsc3->RS_Setup(m_COMPortWL)) != 0)
	{
		std::cout << "RS-232C cannot open port (COM" << m_COMPortWL << ")" << std::endl;
		return FALSE;
	}
	std::cout << "all serial port successfully opened." << std::endl;

	char	m_cCommand[COM_MAX];
	if (!WLonlytest) {

		std::cout << "== Initializing XY stage .... ==" << std::endl;
		ZeroMemory(m_cCommand, COM_MAX);					// Command Clear
		sprintf_s(m_cCommand, sizeof(m_cCommand), "X:1%c%c", CR, LF);			// Set Delimitter to CRLF
		std::cout << "sending Commnd to QT : " << m_cCommand << std::endl;
		rsc0->WriteStringMsc(m_cCommand);				//Send command to QT
	//	rsc1->WriteStringMsc(m_cCommand);				//Send command to QT
	//	rsc2->WriteStringMsc(m_cCommand);				//Send command to QT
	//	rsc0->GoHome();
	}
	std::cout << "== Initializing Auto Focus system .... ==" << std::endl;
	if(!isSimpleBoot)rsc1->AFTest();

	std::cout << "== Initializing Revolver .... ==" << std::endl;
	if(!isSimpleBoot)rsc2->RVTest();

	std::cout << "== Initializing Wafer Loader .... ==" << std::endl;
	rsc3->WaferLoaderTest();



	if (!WLonlytest) {
		SetZoom(10);
		std::cout << "goto original position... " << std::endl;
		//	rsc0->Origin(1);
		//	rsc0->Origin(2);
		getRs232cXYCtrl()->Absolute2D(SCANORIGPOSX, SCANORIGPOSY);
		rsc0->PrintPosition();
	}
	SetScanningPosition();

	//// Open Camera application
	if (!WLonlytest) {
		char szCmd[256] = "C:\\Program Files\\Nikon\\Camera Control Pro 2\\NControlPro.exe";
		std::cout << "opening Camera Control Software : " << std::endl;
		std::cout << szCmd << std::endl;
		OpenApplication(szCmd);
	}

	return 0;
}
int MetrologyScan::OpenApplication(char * szCmd) {
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOA si = { sizeof(STARTUPINFO) };
	int ii=CreateProcessA(NULL, szCmd, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi);
	return ii;
}
void MetrologyScan::SetZoom(int zoom) {
	rsc2->RVSet(zoom);
	rsc1->SetZoom(zoom);
	rsc1->CheckZoom();
	currentzoom = zoom;
}
void MetrologyScan::CheckScanMatrix(float &_startx, float &_starty, float &_stepx, float &_stepy, float &_stopx, float &_stopy) {
	if (_startx < cs0->xposmin) {
		std::cout << "startx(" << _startx << ") is out side of range --> set startx to " << cs0->xposmin << std::endl;
		_startx = cs0->xposmin;
	}
	if (_starty < cs0->yposmin) {
		std::cout << "starty(" << _starty << ") is out side of range --> set starty to " << cs0->yposmin << std::endl;
		_starty = cs0->yposmin;
	}
	if (_stopx > cs0->xposmax) {
		std::cout << "stopx(" << _stopx << ") is out side of range --> set stopx to " << cs0->xposmax << std::endl;
		_stopx = cs0->xposmax;
	}
	if (_stopy > cs0->yposmax) {
		std::cout << "stopy(" << _stopy << ") is out side of range --> set stopy to " << cs0->xposmax << std::endl;
		_stopy = cs0->yposmax;
	}

	int xx = 0;
	int yy = 0;
	while (_startx < xx) xx -= (int)_stepx;
	_startx = xx + _stepx;
	while (_starty < yy) yy -= (int)_stepy;
	_starty = yy + _stepy;
	xx = 0; yy = 0;
	while (_stopx > xx) xx += (int)_stepx;
	_stopx = xx - _stepx;
	while (_stopy > yy) yy += (int)_stepy;
	_stopy = yy - _stepy;
	std::cout << "x : " << _startx << " - " << _stopx << "  # of point " << (_stopx - _startx) / _stepx << std::endl;
	std::cout << "y : " << _starty << " - " << _stopy << "  # of point " << (_stopy - _starty) / _stepy << std::endl;

}
int MetrologyScan::PatternMatchingScan() {
	cs0->setScanOrigCorrection(0,0);
	// move to scan origin position
	float pos[2];
	cs0->getScanPosFromAxis(0, 0, pos);
	std::cout << pos[0] << " " << pos[1] << std::endl;
	getRs232cXYCtrl()->Absolute2D(pos[0], pos[1]);
	getRs232cXYCtrl()->PrintPosition();

	// focus at 10mm inside
	cs0->getScanPosFromAxis(-1 * STRIPW / 2 *0.9, -1 * STRIPH / 2 *0.9, pos);
	getRs232cXYCtrl()->Absolute2D(pos[0], pos[1]);
	std::cout << pos[0] << " " << pos[1] << std::endl;
	rsc1->SendSC0(1);
	Sleep(200);
	rsc1->CheckStatus();
	Sleep(200);
	rsc1->ClearAF();

	// move position for first pattern matching
	cs0->getScanPosFromAxis(-1 * STRIPW / 2, -1 * STRIPH / 2, pos);
	std::cout << pos[0] << " " << pos[1] << std::endl;
	getRs232cXYCtrl()->Absolute2D(pos[0], pos[1]);
	getRs232cXYCtrl()->PrintPosition();
	getTurtleSwitch()->takepicture();
	Sleep(3000);
	std::string filename = dh->GetLastPictureName();
	int cpos1[2];
//	imatool->SetTemplate("topright");
//	imatool->GetCornerPosition(filename, cpos1);



	//forcus at 10mm inside
	cs0->getScanPosFromAxis(STRIPW / 2 * 0.9 , STRIPH / 2 * 0.9, pos);
	getRs232cXYCtrl()->Absolute2D(pos[0], pos[1]);
	std::cout << pos[0] << " " << pos[1] << std::endl;
	rsc1->SendSC0(1);
	Sleep(200);
	rsc1->CheckStatus();
	Sleep(200);
	rsc1->ClearAF();

	// move position for second pattern matching
	cs0->getScanPosFromAxis( STRIPW / 2, STRIPH / 2, pos);
	std::cout << pos[0] << " " << pos[1] << std::endl;
	getRs232cXYCtrl()->Absolute2D(pos[0], pos[1]);
	getRs232cXYCtrl()->PrintPosition();
	getTurtleSwitch()->takepicture();
	Sleep(3000);
	filename = dh->GetLastPictureName();
	int cpos2[2];
//	imatool->SetTemplate("bottomleft");
//	imatool->GetCornerPosition(filename, cpos2);


	std::cout << cpos1[0] << " " << cpos1[1] << std::endl;
	std::cout << cpos2[0] << " " << cpos2[1] << std::endl;

	float dX = ((PHOTOW - cpos1[0]) - cpos2[0]) / 2.;
	float dY = ((PHOTOH - cpos1[1]) - cpos2[1]) / 2.;
	std::cout << dX * PIXELSIZE/2 << " " << dY * PIXELSIZE/2 << std::endl;
	int origdx = (int)(dX * PIXELSIZE/2) / 4 * 4;
	int origdy = (int)(dY * PIXELSIZE/2) / 4 * 4;
	std::cout << "correction : " << origdx << " " << origdy << std::endl;
	cs0->setScanOrigCorrection(origdx, origdy);
	return 0;
}
int MetrologyScan::RunScan(float _startx, float _starty, float _stepx, float _stepy, float _stopx, float _stopy) {
	// type 0 : scan without re-focus
	// type 1 : photo scan without re-focus
	// type 2 : scan with re-focus
	// type 3 : photo scan with re-focus
	int type = scanmode;
	std::ofstream ofs("C:\\work\\ATLASproduction\\ROOTAnalyzer\\log.txt", std::ofstream::out);
	ofs.close();
	CheckScanMatrix(_startx,_starty,_stepx,_stepy,_stopx,_stopy);
	// move to scan origin position
	float pos[2];
	cs0->getScanPosFromAxis(0, 0, pos);
	std::cout << pos[0] << " " << pos[1] << std::endl;
	getRs232cXYCtrl()->Absolute2D(pos[0], pos[1]);
	getRs232cXYCtrl()->PrintPosition();

	for (int ii = 0; ii < 5; ii++) {
		rsc1->SendSC0(1);
		Sleep(200);
	}
	rsc1->CheckStatus();
	Sleep(200);
	double z0pos = rsc1->GetZPos()*UMPARPULSEAF;
	rsc1->ResumeAF();

	char szCmd2[256] = "C:\\root_v6.16.00\\bin\\root.exe -q C:\\work\\ATLASproduction\\ROOTAnalyzer\\test.C";
	char szCmd3[256] = "C:\\root_v6.16.00\\bin\\root.exe -q C:\\work\\ATLASproduction\\ROOTAnalyzer\\test.C";
	if (docloseplot) {
		std::cout << "opening ROOT : " << std::endl;
		std::cout << szCmd2 << std::endl;
		OpenApplication(szCmd2);
	}
	else {
		std::cout << "opening ROOT : " << std::endl;
		std::cout << szCmd3 << std::endl;
		OpenApplication(szCmd3);

	}
	std::ofstream ofscon("C:\\work\\ATLASproduction\\ROOTAnalyzer\\config.txt", std::ofstream::out);
	ofscon << _startx << " " << _stopx << " " << _stepx << std::endl;
	ofscon << _starty << " " << _stopy << " " << _stepy << std::endl;
	ofscon << z0pos << std::endl;
	ofscon.close();
	int ixx = 0;
	for (float xx = _startx; xx <= _stopx; xx += _stepx) {
		for (float yy = _starty; yy <= _stopy; yy += _stepy) {
			float yy2 = ixx % 2 == 0 ? yy : _stopy - (yy - _starty);
			float pos[2];
			cs0->getScanPosFromAxis(xx, yy2 , pos);
//			std::cout << "check relative->absolute : " << xx << " " << yy2 << " " << pos[0] << " " << pos[1] << std::endl;
			if (getRs232cXYCtrl()->Absolute2D(pos[0], pos[1])) {
				std::cout << "cannot move to : " << xx << " " << yy2 << " " << pos[0] << " " << pos[1] << std::endl;
				return -1;
			}
			getRs232cXYCtrl()->PrintPosition();
			if (type & 0x2) {
				for (int ii = 0; ii < 1; ii++) {
					rsc1->SendSC0(1);
					Sleep(200);
				}
//				rsc1->SendSC0(5);
				Sleep(200);
				rsc1->CheckStatus();
				Sleep(200);
			}
			if (type & 0x1) {
				Sleep(500);
				getTurtleSwitch()->takepicture();
				//Sleep(500);
			}
			int zpos=rsc1->GetZPos();
			std::cout << "--> Zpos : " << zpos << std::endl;
			rsc1->ResumeAF();
			Sleep(150);

			std::ofstream ofs("C:\\work\\ATLASproduction\\ROOTAnalyzer\\log.txt", std::ofstream::out | std::ofstream::app);
			ofs << xx << " " << yy2 << " " << (float)zpos*UMPARPULSEAF << std::endl; 
			ofs.close();
		}
		ixx++;
	}
	std::cout << "goto scan original position... " << std::endl;

//	getRs232cXYCtrl()->Origin(1);
//	getRs232cXYCtrl()->Origin(2);
	getRs232cXYCtrl()->Absolute2D(SCANORIGPOSX, SCANORIGPOSY);
	getRs232cXYCtrl()->PrintPosition();
	Sleep(6000);
	dh->MoveImageToDir("test");
	return 0;
}
int MetrologyScan::LoadOneTray(int itray) {
//	if (getRs232cWLCtrl()->GetTS(itray) != 1) return -3;
//	if (GetStagePosition() != 2) return -2;
	std::stringstream ss; ss.str("");
	ss << "LOAD,1," << itray;
	if(getRs232cWLCtrl()->SendReceiveCommand(ss.str(),500,true)!=0)return -1;
	getRs232cWLCtrl()->SetTS(itray-1, 3);
	getRs232cWLCtrl()->CheckStatus(false, true);
	return 0;
}
int MetrologyScan::UnLoadOneTray(int itray) {
//	if (getRs232cWLCtrl()->GetTS(itray) != 3) return -3;
//	if (GetStagePosition() != 2) return -2;
	std::stringstream ss; ss.str("");
	ss << "UNLOAD,4,1," << itray;
	if (getRs232cWLCtrl()->SendReceiveCommand(ss.str(), 500, true) != 0)return -1;
	getRs232cWLCtrl()->CheckStatus(false, true);
	getRs232cWLCtrl()->SetTS(itray-1, 4);
	return 0;
}
int MetrologyScan::VacuumControl(int state) {
	std::string comstr;
	if (state == 1)comstr = "VACON";
	else if (state == 0)comstr = "VACOFF";
	else {
		std::cout << "unknown status set " << state << std::endl;
		return -1;
	}
	getRs232cWLCtrl()->SendReceiveCommand(comstr, 500, true);
}

/*
void MetrologyScan::ROOTtest() {
	TChanvas * can = new TCanvas("c1","c1",1000,1000);
	can->Draw();
}
*/
