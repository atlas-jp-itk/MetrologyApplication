#include "stdafx.h"
#include "TurtleSwitch.h"
#include <iostream>
#include <Windows.h>
#include "TUSBSSW.h"

TurtleSwitch::TurtleSwitch(int _tid)
{
	setid(_tid);
}


TurtleSwitch::~TurtleSwitch()
{
}
int TurtleSwitch::Open() {
	if (Tusbs01sw_Device_Open(tid))
	{
		std::cerr << "Device open failed (id=" << tid << ")" << std::endl;
		return -1;
	}
	return 0;
}
int TurtleSwitch::Close() {
	Tusbs01sw_Device_Close(tid);
	return 0;
}

void TurtleSwitch::setid(int _tid){
	if (_tid < 5 && _tid >= 0)tid = _tid; 
	else std::cout << "set valid id (0, 1, 2 or 3)" << std::endl; 
}

int TurtleSwitch::takepicture()
{
	Open();
	if (Tusbs01sw_Out(tid, 1))
	{
		std::cout << "failed to switch on device (id=" << tid << ")" << std::endl;
		ison = true;
	}
	Sleep(100);
	if (Tusbs01sw_Out(tid, 0))
	{
		std::cout << "failed to switch off device (id=" << tid << ")" << std::endl;
	}
	Close();
	return 0;
}

int TurtleSwitch::onofftest()
{
	if (Tusbs01sw_Device_Open(tid))
	{
		std::cerr << "Device open failed (id=" << tid << ")" << std::endl;
		return -1;
	}
	else {
		std::cout << "Device successfully opened (id=" << tid << ")" << std::endl;
	}
	if (Tusbs01sw_Out(tid, 1))
	{
		std::cout << "failed to switch on device (id=" << tid << ")" << std::endl;
		ison = true;
	}
	else {
		std::cout << "Device successfully switched on (id=" << tid << ")" << std::endl;
	}
	Sleep(2000);
	if (Tusbs01sw_Out(tid, 0))
	{
		std::cout << "failed to switch off device (id=" << tid << ")" << std::endl;
	}
	else {
		std::cout << "Device successfully switched off (id=" << tid << ")" << std::endl;
		ison = false;
	}

	Tusbs01sw_Device_Close(tid);
	return 0;
}

