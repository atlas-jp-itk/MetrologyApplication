#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "tusbssw.h"
#include <string.h>
#include "TurtleSwitch.h"
#include "Rs232cXYCtrl.h"
#include "sstream"
#include "MetrologyScan.h"
#include "ControlGUI.h"

using namespace System;
using namespace MetrologyScanApp;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{

	/*****************************************************/
	//  Scan position and take picture at each position
	/*****************************************************/
/*
float startx = 0; // [um]
	float starty = 0; // [um]
	float stepx = 3000; // [um]
	float stepy = 3000; // [um]
	float endx = -1; // [um]     negative ment until the max value
	float endy = -1; // [um]     negative ment until the max value
*/
//	int ret = ms0.RunScanPhoto(startx, starty, stepx, stepy, endx, endy);
//	if (ret != 0) {
//		std::cerr << "RunScanPhoto() failed... " << std::endl;
//		return ret;
//	}

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew ControlGUI());

    return 0;
}
